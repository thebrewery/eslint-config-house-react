module.exports = {
	"extends": "@house-agency/eslint-config-house-foundation",
	"plugins": [
		"react"		// https://github.com/yannickcr/eslint-plugin-react
	],
	"rules": {
		"react/sort-comp": ["error", {
			"order": ["type-annotations", "static-methods", "lifecycle", "everything-else", "render"]
		}],
		"react/jsx-filename-extension": ["error", { "extensions": [".js", ".jsx"] }],
		"react/prefer-stateless-function": "off",
		"react/jsx-curly-spacing": [2, "always"],
		"react/jsx-closing-tag-location": 2
	}
}
