# eslint-config-house-react

## Usage

`npm i @house-agency/eslint-config-house-react -D`

Then add a `.eslintrc` file to your project that contains:

```
{
  "extends": "@house-agency/eslint-config-house-react"
}
```